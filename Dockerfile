FROM maven:alpine
EXPOSE 8080
WORKDIR /opt
COPY . .
RUN mvn --show-version -B install
WORKDIR /opt/webservice
CMD mvn --show-version exec:java -Dexec.mainClass="de.philipp1994.lunchmenu.webservice.LunchMenuWebserviceLauncher"
