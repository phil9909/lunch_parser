package de.philipp1994.lunchmenu.webservice.dialogflow;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import de.philipp1994.lunch.common.ILunchMenuProvider;
import de.philipp1994.lunch.common.LunchMenuItem;
import de.philipp1994.lunch.common.LunchProviderException;
import de.philipp1994.lunch.common.prefs.IUserPreferences;
import de.philipp1994.lunch.common.prefs.Preference;
import de.philipp1994.lunchmenu.webservice.LunchMenuServlet;

public class DialogflowServlet extends HttpServlet{
	
	private static final long serialVersionUID = -4901329239073236030L;

	private Gson gson;
	
	@Override
	public void init() throws ServletException {
		super.init();
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(LocalDate.class, new TypeAdapter<LocalDate>() {
			@Override
			public void write(JsonWriter out, LocalDate value) throws IOException {
				out.value(value.toString());
			}

			@Override
			public LocalDate read(JsonReader in) throws IOException {
				try {
					return LocalDate.parse(in.nextString(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
				}
				catch(DateTimeParseException ex) {
					throw new JsonIOException(ex);
				}
			}
		});
		gson = builder.create();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json; charset=utf-8");
		Reader in = req.getReader();
		PrintStream out = new PrintStream(resp.getOutputStream(), false, "UTF-8");
		handle(in, out);
	}
	

	private static Stream<ILunchMenuProvider> findProvider(List<String> uuids) {
		if(uuids.contains("all")) {
			return Arrays.stream(LunchMenuServlet.PROVIDER).limit(3);
		}
		else {
			return Arrays.stream(LunchMenuServlet.PROVIDER).filter(provider -> {
				return uuids.contains(provider.getUUID().toString());
			});
		}
	}
	
	private void handle(Reader in, PrintStream out) {
		DialogflowRequest r = gson.fromJson(in, DialogflowRequest.class);
		
		System.out.println("id: " + r.responseId);
		System.out.println("action: " + r.queryResult.action);
		
		final LocalDate date = r.queryResult.parameters.date;
		
		IUserPreferences prefs = new IUserPreferences() {
			
			@Override
			public <T> T getValue(Preference<T> preference) throws ClassCastException, NoSuchElementException {
				return preference.getDefaultValue();
			}
		};

		String text = findProvider(r.queryResult.parameters.mensa)
			.parallel()
			.flatMap(p -> {
				try {
					return p.getMenu(date, prefs).stream();
				} catch (IOException | LunchProviderException e) {
					e.printStackTrace();
					return null;
				}
			})
			.filter(o -> o != null)
			.map(m -> {
				String res = "bei " + m.getName() + ": ";
				String items = m.getLunchItems().stream()
					.map(LunchMenuItem::getItemName)
					.reduce((a,b) -> a + ", " + b)
					.orElse("nix");
				return res + items;
			})
			.map(String::trim)
			.reduce((a,b) -> a + "; " + b).orElse("nichts");
		
		System.out.println(text);
		DialogflowResponse result = new DialogflowResponse();
		result.fulfillmentText = text;
		
		gson.toJson(result, out);
	}

}
