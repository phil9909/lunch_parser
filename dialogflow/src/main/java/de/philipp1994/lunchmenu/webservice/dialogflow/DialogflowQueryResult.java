package de.philipp1994.lunchmenu.webservice.dialogflow;

import java.time.LocalDate;
import java.util.List;

public class DialogflowQueryResult {
	
	public static class DialogflowQueryResultParameter {
		LocalDate date;
		List<String> mensa;
	}
	
	String queryText;
	String action;
	DialogflowQueryResultParameter parameters;
	boolean allRequiredParamsPresent;
	
	/* TODO: 
  	"fulfillmentMessages": [
	      {
	        "text": {
	          "text": [
	            ""
	          ]
	        }
	      }
	    ],
	    "intent": {
	      "name": "projects/lunch-menu-e1c4a/agent/intents/47124e7a-592a-42ce-b24e-5e444e0b327f",
	      "displayName": "Was gibt es heute zu Essen?"
	    },
	    "intentDetectionConfidence": 1,
	    "languageCode": "de"
	  */
}