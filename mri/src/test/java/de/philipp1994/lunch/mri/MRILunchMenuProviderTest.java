package de.philipp1994.lunch.mri;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import de.philipp1994.lunch.common.LunchMenu;
import de.philipp1994.lunch.common.LunchMenuItem;

public class MRILunchMenuProviderTest {
	
	private class LunchMenuItemMatcher implements Predicate<LunchMenuItem> {
		
		private final String name;
		private final double price;
		
		public LunchMenuItemMatcher(String name, double price) {
			this.name = name;
			this.price = price;
		}

		@Override
		public boolean test(LunchMenuItem item) {
			if(item == null) {
				return false;
			}

			return
				name == null ? (item.getItemName() == null) : (name.equals(item.getItemName())) &&
				price == item.getPrice();
		}
		
	}
	
	private static void assertInMenu(LunchMenu menu, LunchMenuItemMatcher matcher) {
		boolean isPresent = menu
			.getLunchItems()
			.stream()
			.filter(matcher)
			.findFirst()
			.isPresent();
		
		if(!isPresent) {
			fail(String.format("Expected Lunch Menu Item (name: %s price: %f) not found in parsed menu", matcher.name, matcher.price));
		}
	}
	
	private InputStream getTestData(String name) {
		InputStream in = this.getClass().getClassLoader().getResourceAsStream(name + ".html");
		assertNotNull(in, "Test data not available");
		return in;
	}

	@Test
	public void testParsing() throws IOException {
		InputStream testData = getTestData("test_2018_KW36_montag");
		LunchMenu menu = new MRILunchMenuProvider().parse(testData, LocalDate.of(2018, 9, 3));
		
		assertNotNull(menu);
		
		assertEquals(3, menu.getLunchItems().size());
		assertInMenu(menu, new LunchMenuItemMatcher("Fritatta mit Zucchini, Feta und Tomaten", 4.9));
		assertInMenu(menu, new LunchMenuItemMatcher("Schweineschnitzel paniert mit Pommes Frittes", 5.4));
		assertInMenu(menu, new LunchMenuItemMatcher("Curry wurst mit Pommes frites", LunchMenuItem.PRICE_UNKOWN));
		

		testData = getTestData("test_2018_KW38_dienstag");
		menu = new MRILunchMenuProvider().parse(testData, LocalDate.of(2018, 9, 18));
		
		assertNotNull(menu);
		
		assertEquals(3, menu.getLunchItems().size());
		assertInMenu(menu, new LunchMenuItemMatcher("Pasta mit Gemüsestreifen und Tomatensauce", 4.9));
		assertInMenu(menu, new LunchMenuItemMatcher("Ungarisches Rindergoulasch mit Kartoffelpüree", 5.4));
//		WONTFIX: assertInMenu(menu, new LunchMenuItemMatcher("Kassler mit Kroketen und braten Sauce", LunchMenuItem.PRICE_UNKOWN));
	}
}
